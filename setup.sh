#!/bin/bash

## The following piece of script will add a GPGKey and make sure that git will use it
[[ ! -z $GPGKEY  ]] &&
gpg --verbose --batch --import <(echo $GPGKEY|base64 -d) &&
echo 'pinentry-mode loopback' >> ~/.gnupg/gpg.conf &&
export GPGKEY_ID=$(gpg --list-secret-keys --with-colons --fingerprint | awk -F: '$1 == "fpr" {print $10;}' | head -1) &&
git config --global user.signingkey $GPGKEY_ID &&
git config --global commit.gpgsign true &&
git config --global user.email "gmail@osouza.de" &&
git config --global user.name "Anderson de Souza" &&
git config --global gpg.program "gpg"
